

$( document ).ready(function() {
    $('.fecha-desde').datepicker({
        format: 'mm-dd-yyyy',
        clearBtn: "true",
        startDate: new Date(),
        autoclose: "true",
        orientation: "bottom",
      }).on('changeDate', function (selected) {
          var minDate = new Date(selected.date.valueOf());
          $('.fecha-hasta').datepicker('setStartDate', minDate);
      });
    $('.fecha-hasta').datepicker({
        format: 'mm-dd-yyyy',
        clearBtn: "true",
        autoclose: "true",
        orientation: "bottom",
        startDate: new Date(),
      });

});



$('.carousel').carousel();