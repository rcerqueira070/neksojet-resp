function remove_modal_errors(){
    $("#email_errors ul > li").remove()
    $("#name_errors ul > li").remove()
    $("#reason_errors ul > li").remove()
    $("#phone_errors ul > li").remove()
}


$(".btn-nekso-mobile, .btn-nekso, .btn-mobile-registrate, .btn-registrate").on('click',function(){
    $("#name").val("")
    $("#email").val("")
    $("#phone").val("")
    $("#reason").val("")
    remove_modal_errors()
    $(".btn-nekso-submit").prop('disabled', false);
    $("#contactModal").modal('show');
});

$( document ).ready(function() {
    $('.fecha-desde').datepicker({
        format: 'mm-dd-yyyy',
        clearBtn: "true",
        startDate: new Date(),
        autoclose: "true",
        orientation: "bottom",
      }).on('changeDate', function (selected) {
          var minDate = new Date(selected.date.valueOf());
          $('.fecha-hasta').datepicker('setStartDate', minDate);
      });
    $('.fecha-hasta').datepicker({
        format: 'mm-dd-yyyy',
        clearBtn: "true",
        autoclose: "true",
        orientation: "bottom",
        startDate: new Date(),
      });
    $('.fecha-desde-mobile').datepicker({
        format: 'mm-dd-yyyy',
        clearBtn: "true",
        startDate: new Date(),
        autoclose: "true",
        orientation: "bottom",
      }).on('changeDate', function (selected) {
          var minDate = new Date(selected.date.valueOf());
          $('.fecha-hasta-mobile').datepicker('setStartDate', minDate);
      });
    $('.fecha-hasta-mobile').datepicker({
        format: 'mm-dd-yyyy',
        clearBtn: "true",
        autoclose: "true",
        orientation: "bottom",
        startDate: new Date(),
      });

});




function submit_form(){
    // console.log("holis")
    // data = $('#form_contact').serialize()
    // debugger
    $(".btn-nekso-submit").prop('disabled', true);
    $.ajax({
        dataType: "json",
        url: '/',
        data: $('#form_contact').serialize(),
        type: 'POST',
        success: function(response) {
            console.log(response);
            remove_modal_errors()
            if(response['result'] == "EMPTY_DATA"){
                // console.log('holias')
                if(response.email['errors'].length){
                    for (i=0; i<response.email['errors'].length; i++){
                        $("#email_errors ul").append('<li>'+response.email['errors'][i]+'</li>')
                    }
                }
                if(response.name['errors'].length){
                    for (i=0; i<response.name['errors'].length; i++){
                        $("#name_errors ul").append('<li>'+response.name['errors'][i]+'</li>')
                    }
                }
                if(response.reason['errors'].length){
                    for (i=0; i<response.reason['errors'].length; i++){
                        $("#reason_errors ul").append('<li>'+response.reason['errors'][i]+'</li>')
                    }
                }
                if(response.phone['errors'].length){
                    for (i=0; i<response.phone['errors'].length; i++){
                        $("#phone_errors ul").append('<li>'+response.phone['errors'][i]+'</li>')
                    }
                }
                $(".btn-nekso-submit").prop('disabled', false);
            }
            if(response['result'] == "success"){
                $("#contactModal").modal('hide');
                $("#feedBackModal").modal('show');
            }
        },
        error: function(error) {
            $(".btn-nekso-submit").prop('disabled', false);
            
            console.log(error);
        }
    });

}