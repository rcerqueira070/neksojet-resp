from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_moment import Moment
from flask_debugtoolbar import DebugToolbarExtension
from flask_alchemydumps import AlchemyDumps, AlchemyDumpsCommand
from flask_bootstrap import Bootstrap
from flask_mail import Mail

from .config import config_by_name
db = SQLAlchemy()

#configure authentication
login_manager = LoginManager()
login_manager.session_protection = "strong"
login_manager.login_view = "main.index"

#enable debugtoolbar
toolbar = DebugToolbarExtension()

# for displaying timestamps
moment = Moment()

#AlchemyDumps

alchemydumps = AlchemyDumps()
bootstrap = Bootstrap()

mail = Mail()

def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])
    alchemydumps.init_app(app, db)
    db.init_app(app)
    login_manager.init_app(app)
    moment.init_app(app)
    toolbar.init_app(app)
    bootstrap.init_app(app)
    mail.init_app(app)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint, url_prefix='/')
    
    return app