import os

basedir= os.path.abspath(os.path.dirname(__file__))


class Config:
    SECRET_KEY = '\x05\xa5\xa6\xb8\xe8\x06\x0f\xa9\x98\xf4\xf8'+\
                 '\xe5H\x92j9\xab\x16\xe5\xe0\xa8\x9e\xb9\x92'
    DEBUG = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # mailconfig
    MAIL_SERVER = 'smtp.office365.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = "neksojet@nekso.io"
    MAIL_PASSWORD = "!Kumu0183"
    MAIL_DEFAULT_SENDER = "neksojet@nekso.io"

class DevelopmentConfig(Config):
    DEBUG = True
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:123456@localhost:5432/' +\
                              'neksojeti'
    # SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:123456@localhost/neksojet'


class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:123456@localhost:5432/' +\
                              'neksojet'
    WTF_CSRF_ENABLED = False

class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:123456@localhost:5432/' +\
                              'neksojet'


config_by_name = dict(
    dev = DevelopmentConfig,
    test = TestingConfig,
    prod = ProductionConfig
)
