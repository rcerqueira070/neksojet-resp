from neksojet import db
from sqlalchemy import desc
from datetime import datetime


class Contact(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    email = db.Column(db.String(120))
    phone =  db.Column(db.String(200),nullable= True)
    reason = db.Column(db.Text())

    def __repr__(self):
        return "<Contact '{}'>".format(self.name)