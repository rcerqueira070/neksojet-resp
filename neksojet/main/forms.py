from flask_wtf import FlaskForm as Form
from wtforms.fields import StringField, PasswordField, BooleanField,SubmitField
from wtforms.fields.html5 import URLField
from wtforms.validators import DataRequired, url, Length, Email, Regexp, \
                        EqualTo, ValidationError
from .models import Contact 
from wtforms_alchemy import PhoneNumberField,Unique
from wtforms_components import If, Chain
from flask_login import current_user
from wtforms.widgets import TextArea
from wtforms.fields.html5 import TelField, EmailField

class ContactForm(Form):
    name = StringField(
                    'Name',
                    validators=[
                        DataRequired(), Length(3,80)
                    ])
    email = EmailField(
                    'Email', validators=[DataRequired(), 
                    Length(1,120),Email()])
    phone = StringField('Phone number')
    reason =  StringField('Reason of contact',validators=[DataRequired()],widget=TextArea())