from flask import render_template, request, json
from . import main 
from .. import login_manager
from .. import db
from .. import mail
from .models import Contact
from .forms import ContactForm
from flask_mail import Message, Mail
# from flask import mail

# 
# mail = Mail(app)

@login_manager.user_loader
def load_user(userid):
    return Contact.query.get(int(userid))

@main.route('/index', methods=['GET', 'POST'])
@main.route('/', methods=['GET', 'POST']) 
def index():
    form = ContactForm()
    # import pudb; pu.db
    if request.method == 'POST':
        if form.validate_on_submit():
            user = Contact(
                        name = form.name.data,
                        email = form.email.data,
                        phone=form.phone.data,
                        reason=form.reason.data)
            db.session.add(user)
            db.session.commit()

            # enviar mail
            name = form.name.data
            email = form.email.data
            phone=form.phone.data
            reason=form.reason.data
            html = '<p>Name: '+name+'</p>' 
            html += '<p>Email: '+email+'</p>' 
            if phone:
                html += '<p>Phone: '+phone+'</p>'
            html += '<p>Reason: '+reason+'</p>' 

            subject = "[New contact in neksojet]"
            msg = Message(recipients=["neksojet@nekso.io"],
                          html=html,
                          subject=subject)
            mail.send(msg)
            # end sending email

            return json.dumps({'result':'success'});
        else:
            email = {}
            name = {}
            reason = {}
            phone = {}
            email['errors'] = form.email.errors
            name['errors'] = form.name.errors
            reason['errors'] = form.reason.errors
            phone['errors'] = form.phone.errors
            context = {
                'email':email,
                'name':name,
                'reason':reason,
                'phone':phone,
                'result':'EMPTY_DATA'
            }

            return json.dumps(context);
    if request.method == 'GET':
        return render_template('main/corporate.html', form=form)

        