#! /usr/bin/evn python 

import os 
import sys

from neksojet import create_app , db

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from flask_alchemydumps import AlchemyDumps, AlchemyDumpsCommand

app = create_app(os.getenv('NEKSOJET_ENV') or 'dev')
manager = Manager(app)

migrate = Migrate(app,db)

manager.add_command('db', MigrateCommand)
manager.add_command('alchemydumps', AlchemyDumpsCommand)

if __name__ == '__main__':
    # from configurations.management import execute_from_command_line

    # execute_from_command_line(sys.argv)
    manager.run()


