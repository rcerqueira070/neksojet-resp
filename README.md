# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Neksojet Proyect

### How do I get set up? ###
    
    * Summary of set up

        - python need to be installed
        - install virtualenv
        - install postgresql 9.6

    * Dependencies
        - create virtualenv called neksojet
        - install in the virtualenv with pip
            - pip install -r requirements.txt
	
    * Database configuration
        - Create a postgres db with the name "neksojet"
            > PGPASSWORD=<pass> createdb -h <serv> -p <port> -U postgres "neksojet"
        - Change in "neksojet -> config.py" the database information in ProductionConfig(Config)
        - Change in manage.py "app = create_app(os.getenv('NEKSOJET_ENV') or 'dev')" to "app = create_app(os.getenv('NEKSOJET_ENV') or 'prod')"
        - run 
            > python manage.py db upgrade

    * Runserver
        - Run server with the command "python manage.py runserver" in the folder where is manage.py
        - the application runs on http://localhost:5000/

### Contribution guidelines ###

Flask-sqlalchemy
    - pythonhosted.org/Flask-SQLAlchemy

SQLAlchemy
    - docs.sqlalchemy.org/en/latest

Flask-wtf
    - https://flask-wtf.readthedocs.io/en/stable/

Flask Script
    - http://flask-script.readthedocs.org/en/latest/

Flask Login
	- http://flask-login.readthedocs.io/en/latest/

Moment
    - https://github.com/miguelgrinberg/flask-moment

Select2
    - https://ivaynberg.github.io/select2/

Jinja filters
    - http://jinja.pocoo.org

flask_debugtoolbar
    - https://github.com/mgood/flask-debugtoolbar

Flask-Testing
    - http://pythonhosted.org/Flask-Testing/

nose: test runner
    - https://nose.readthedocs.org/en/latest/

blueprint
    - http://flask.pocoo.org/docs/latest/blueprints/

Appfactories
    - http://flask.pocoo.org/docs/latest/patterns/appfactories/

Config
    - http://flask.pocoo.org/docs/latest/config



### Who do I talk to? ###

- ricardoc@blanclabs.com